/*
* Christina Zhao
* INFO/CS 1300
* Fall 2016
*/

$(document).ready(function() {
    $("#footer").click(function() {
        $(this).hide();
    });
    $("#car").click(function() {
        $("#car").animate({marginLeft: "700px"}, 1000);
    });
});

function animate_ghost() {
    "use strict";
   
    //ghost is visible
    if($("#ghost").is(':visible')){
        $("#ghost").fadeOut(3000);
    }
    
    //ghost is hidden
    if(!$("#ghost").is(':visible')){
        $("#ghost").fadeIn(3000);
    }

}

function setFallTheme(){
    "use strict";
    $("#nav").css("background-color", "#FF8C00");
    $("#background").css("background-color", "#F4A460");
}

function setSpringTheme(){
    "use strict";
    $("#nav").css("background-color", "#1C4905");
    $("#background").css("background-color", "#D8ECC3");
}